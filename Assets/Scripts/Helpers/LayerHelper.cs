﻿using UnityEngine;
using System.Collections.Generic;

namespace RTEditor
{
    /// <summary>
    /// Contains useful functions which can be used when working with layers.
    /// </summary>
    public static class LayerHelper
    {
        #region Public Static Functions
        /// <summary>
        /// Returns the names of all layers.
        /// </summary>
        /// <remarks>
        /// The function returns only the names of the layers which have been given a
        /// name inside the Unity Editor.
        /// </remarks>
        public static List<string> GetAllLayerNames()
        {
            // Loop through each layer
            var layerNames = new List<string>();
            for (int layerIndex = 0; layerIndex < 31; ++layerIndex)
            {
                // Retrieve the name and if it is valid, store it inside the layer name list
                string layerName = LayerMask.LayerToName(layerIndex);
                if (!string.IsNullOrEmpty(layerName)) layerNames.Add(layerName);
            }

            // Return the layer name list
            return layerNames;
        }
        #endregion
    }
}
