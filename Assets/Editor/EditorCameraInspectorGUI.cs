﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace RTEditor
{
    /// <summary>
    /// Custom inspector implementation for the 'EditorCamera' class.
    /// </summary>
    [CustomEditor(typeof(EditorCamera))]
    public class EditorCameraInspectorGUI : Editor
    {
        #region Private Variables
        /// <summary>
        /// Reference to the currently selected editor camera.
        /// </summary>
        private EditorCamera _editorCamera;

        /// <summary>
        /// The following variables control the visibility for different categories of settings.
        /// </summary>
        private static bool _zoomSettingsAreVisible = true;
        private static bool _panSettingsAreVisible = true;
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the inspector needs to be rendered.
        /// </summary>
        public override void OnInspectorGUI()
        {
            float newFloatValue;
            const int indentLevel = 1;

            // Let the user control the visibility of the zoom settings
            _zoomSettingsAreVisible = EditorGUILayout.Foldout(_zoomSettingsAreVisible, "Zoom Settings");
            if(_zoomSettingsAreVisible)
            {
                EditorCameraZoomSettings zoomSettings = _editorCamera.ZoomSettings;
                EditorGUI.indentLevel += indentLevel;

                // Let the user specify the camera zoom mode
                EditorCameraZoomMode newZoomMode = (EditorCameraZoomMode)EditorGUILayout.EnumPopup("Zoom Mode", zoomSettings.ZoomMode);
                if (newZoomMode != zoomSettings.ZoomMode)
                {
                    UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                    zoomSettings.ZoomMode = newZoomMode;
                }

                if(newZoomMode == EditorCameraZoomMode.Smooth)
                {
                    // Let the user choose a smooth value for both camera types
                    EditorGUILayout.Separator();
                    newFloatValue = EditorGUILayout.Slider("Orthographic Smooth Value", zoomSettings.OrthographicSmoothValue, EditorCameraZoomSettings.MinSmoothValue, 1.0f);
                    if (newFloatValue != zoomSettings.OrthographicSmoothValue)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.OrthographicSmoothValue = newFloatValue;
                    }

                    newFloatValue = EditorGUILayout.Slider("Perspective Smooth Value", zoomSettings.PerspectiveSmoothValue, EditorCameraZoomSettings.MinSmoothValue, 1.0f);
                    if (newFloatValue != zoomSettings.PerspectiveSmoothValue)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.PerspectiveSmoothValue = newFloatValue;
                    }
                }
             
                if (newZoomMode == EditorCameraZoomMode.Standard)
                {
                    // Let the user specify the zoom speed when the camera operates in ortho mode and the zoom mode is set to 'Standard'
                    EditorGUILayout.Separator();
                    newFloatValue = EditorGUILayout.FloatField("Orthographic Standard Zoom Speed", zoomSettings.OrthographicStandardZoomSpeed);
                    if (newFloatValue != zoomSettings.OrthographicStandardZoomSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.OrthographicStandardZoomSpeed = newFloatValue;
                    }

                    // Let the user specify the zoom speed when the camera operates in perspective mode and the zoom mode is set to 'Standard'
                    newFloatValue = EditorGUILayout.FloatField("Perspective Standard Zoom Speed", zoomSettings.PerspectiveStandardZoomSpeed);
                    if (newFloatValue != zoomSettings.PerspectiveStandardZoomSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.PerspectiveStandardZoomSpeed = newFloatValue;
                    }
                }

                if (newZoomMode == EditorCameraZoomMode.Smooth)
                {
                    // Let the user specify the zoom speed when the camera operates in ortho mode and the zoom mode is set to 'Smooth'
                    EditorGUILayout.Separator();
                    newFloatValue = EditorGUILayout.FloatField("Orthographic Smooth Zoom Speed", zoomSettings.OrthographicSmoothZoomSpeed);
                    if (newFloatValue != zoomSettings.OrthographicSmoothZoomSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.OrthographicSmoothZoomSpeed = newFloatValue;
                    }

                    // Let the user specify the zoom speed when the camera operates in perspective mode and the zoom mode is set to 'Smooth'
                    newFloatValue = EditorGUILayout.FloatField("Perspective Smooth Zoom Speed", zoomSettings.PerspectiveSmoothZoomSpeed);
                    if (newFloatValue != zoomSettings.PerspectiveSmoothZoomSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        zoomSettings.PerspectiveSmoothZoomSpeed = newFloatValue;
                    }
                }

                EditorGUI.indentLevel -= indentLevel;
            }

            // Let the user specify the camera pan speed
            _panSettingsAreVisible = EditorGUILayout.Foldout(_panSettingsAreVisible, "Pan Settings");
            if(_panSettingsAreVisible)
            {
                EditorCameraPanSettings panSettings = _editorCamera.PanSettings;
                EditorGUI.indentLevel += indentLevel;

                // Let the user choose the pan mode
                EditorCameraPanMode newPanMode = (EditorCameraPanMode)EditorGUILayout.EnumPopup("Pan Mode", panSettings.PanMode);
                if(newPanMode != panSettings.PanMode)
                {
                    UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                    panSettings.PanMode = newPanMode;
                }

                if(panSettings.PanMode == EditorCameraPanMode.Smooth)
                {
                    // Let the user choose the pan smooth value
                    EditorGUILayout.Separator();
                    newFloatValue = EditorGUILayout.Slider("Smooth Value", panSettings.SmoothValue, EditorCameraPanSettings.MinSmoothValue, 1.0f);
                    if(newFloatValue != panSettings.SmoothValue)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        panSettings.SmoothValue = newFloatValue;
                    }

                    // Let the user choose the smooth pan speed
                    newFloatValue = EditorGUILayout.FloatField("Smooth Pan Speed", panSettings.SmoothPanSpeed);
                    if (newFloatValue != panSettings.SmoothPanSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        panSettings.SmoothPanSpeed = newFloatValue;
                    }
                }
                else
                {
                    // Let the user choose the standard pan speed
                    newFloatValue = EditorGUILayout.FloatField("Standard Pan Speed", panSettings.StandardPanSpeed);
                    if (newFloatValue != panSettings.StandardPanSpeed)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                        panSettings.StandardPanSpeed = newFloatValue;
                    }
                }
                EditorGUI.indentLevel -= indentLevel;
            }

            // Let the user specify the camera rotation speed in degree units
            EditorGUILayout.Separator();
            newFloatValue = EditorGUILayout.FloatField("Rotation Speed (Degrees)", _editorCamera.RotationSpeedInDegrees);
            if (newFloatValue != _editorCamera.RotationSpeedInDegrees)
            {
                UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorCamera);
                _editorCamera.RotationSpeedInDegrees = newFloatValue;
            }
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Called when the editor camera is selected in the scene view.
        /// </summary>
        protected virtual void OnEnable()
        {
            _editorCamera = target as EditorCamera;
        }
        #endregion
    }
}
#endif
