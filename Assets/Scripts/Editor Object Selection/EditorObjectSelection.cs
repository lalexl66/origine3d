﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace RTEditor
{
    /// <summary>
    /// This class implements the object selection functionality. It also acts as
    /// a container for all the currently selected objects.
    /// </summary>
    [Serializable]
    public class EditorObjectSelection : MonoSingletonBase<EditorObjectSelection>
    {
        #region Private Variables
        /// <summary>
        /// This class holds the settings which are needed when rendering the object 
        /// selection boxes.
        /// </summary>
        [SerializeField]
        private ObjectSelectionBoxDrawSettings _objectSelectionBoxDrawSettings = new ObjectSelectionBoxDrawSettings();

        /// <summary>
        /// Holds the currently selected objects.
        /// </summary>
        private HashSet<GameObject> _selectedObjects = new HashSet<GameObject>();

        /// <summary>
        /// This is the object selection mask. Objects added to the selection mask can not be selected.
        /// </summary>
        private HashSet<GameObject> _maskedObjects = new HashSet<GameObject>();

        /// <summary>
        /// This is the object selection rectangle which can be used to select or deselect multiple
        /// objects at once.
        /// </summary>
        [SerializeField]
        private ObjectSelectionRectangle _objectSelectionRectangle = new ObjectSelectionRectangle();

        /// <summary>
        /// This is the material which is used to render any necessary lines. For example, we will
        /// use this to draw the object selection boxes.
        /// </summary>
        private Material _lineRenderingMaterial;

        /// <summary>
        /// This is a reference to the last game object which was selected by the user. 
        /// </summary>
        private GameObject _lastSelectedGameObject;

        /// <summary>
        /// This variable represents the average of all world centers of the selected game objects. It
        /// represents the world space center of the selection.
        /// </summary>
        private Vector3 _selectionWorldCenter;

        /// <summary>
        /// When this is true, objects will be added to the selection when an object is clicked or
        /// when the selection shape intersects any objects.
        /// </summary>
        private bool _addObjectsToSelectionEnabled;

        /// <summary>
        /// When this is true, it means the user wants to deselect objects using the object selection
        /// shape.
        /// </summary>
        private bool _multiObjectDeselectEnabled;

        /// <summary>
        /// The following 3 variables are used in conjunction with multiple object selection (i.e. object
        /// selection using the object selection shape). When the user presses the left mouse button and
        /// starts moving the mouse around, objects may get selected/deselected if they fall inside the 
        /// area of the object selection shape. When the left mouse button is released a post object selection
        /// change action must be executed and these 3 variables allow us to do that.
        /// </summary>
        private ObjectSelectionSnapshot _preMultiObjectSelectionChangeSnapshot;
        private ObjectSelectionSnapshot _postMultiObjectSelectionChangeSnapshot;
        private bool _multiSelectionWasPerformedSinceLeftMouseButtonWasPressed;

        /// <summary>
        /// We will need this to get access to necessary mouse data.
        /// </summary>
        private Mouse _mouse = new Mouse();
        #endregion

        #region Public Properties
        /// <summary>
        /// Returns the object selection box draw settings.
        /// </summary>
        public ObjectSelectionBoxDrawSettings ObjectSelectionBoxDrawSettings { get { return _objectSelectionBoxDrawSettings; } }

        /// <summary>
        /// Returns the object selection rectangle draw settings.
        /// </summary>
        public ObjectSelectionRectangleDrawSettings ObjectSelectionRectangleDrawSettings { get { return _objectSelectionRectangle.DrawSettings; } }

        /// <summary>
        /// Returns the last game object which was selected by the user.
        /// </summary>
        /// <remarks>
        /// When objects are removed from the selection, this will return an arbitrary object from
        /// the selection collection. This is because a hash set is used to store the game objects,
        /// so the order of the game objects is not preserved. 
        /// </remarks>
        public GameObject LastSelectedGameObject { get { return _lastSelectedGameObject; } }

        /// <summary>
        /// Returns a copy of the internal selected objects collection.
        /// </summary>
        public HashSet<GameObject> SelectedGameObjects { get { return new HashSet<GameObject>(_selectedObjects); } }

        /// <summary>
        /// Returns a copy of the internal object selection mask collection.
        /// </summary>
        public HashSet<GameObject> MaskedObjects { get { return new HashSet<GameObject>(_maskedObjects); } }

        /// <summary>
        /// Returns the number of selected game objects.
        /// </summary>
        public int NumberOfSelectedObjects { get { return _selectedObjects.Count; } }

        /// <summary>
        /// Gets/sets the boolean flag which specifies whether or not the user can add objects to the
        /// current selection by clicking on them or by using the object selection shape.
        /// </summary>
        public bool AddObjectsToSelectionEnabled { get { return _addObjectsToSelectionEnabled; } set { _addObjectsToSelectionEnabled = value; } }

        /// <summary>
        /// Gets/sets the boolean flag which specifies whether or not the user can deselect objects 
        /// using the object selection shape.
        /// </summary>
        public bool MultiObjectDeselectEnabled { get { return _multiObjectDeselectEnabled; } set { _multiObjectDeselectEnabled = value; } }
        #endregion

        #region Public Methods
        /// <summary>
        /// Checks if the specified game object is selected.
        /// </summary>
        public bool IsGameObjectSelected(GameObject gameObject)
        {
            return _selectedObjects.Contains(gameObject);
        }

        /// <summary>
        /// Adds the specified game object collection to the selection mask. The method returns true if adding 
        /// the game objects to the mask causes the selection to change. For example, if the collection contains
        /// the objects A and B, and A is currently selected, the method will deselect it. This will cause a 
        /// selection change.
        /// </summary>
        public bool AddGameObjectCollectionToSelectionMask(List<GameObject> gameObjects)
        {
            // Loop through all objects
            bool selectionWasChanged = false;
            foreach(GameObject gameObject in gameObjects)
            {
                // If the game object is selected, we have to remove it from the current selection
                if(IsGameObjectSelected(gameObject))
                {
                    // Remove the object from the selection and set the boolean to true
                    _selectedObjects.Remove(gameObject);
                    selectionWasChanged = true;
                }

                // Add the game object to the selection mask
                _maskedObjects.Add(gameObject);
            }

            // If the selection was changed, inform all interested listeners
            if (selectionWasChanged) ObjectSelectionChangedMessage.SendToInterestedListeners();

            return selectionWasChanged;
        }

        /// <summary>
        /// Removes the specified game object collection from the selection mask.
        /// </summary>
        public void RemoveGameObjectCollectionFromSelectionMask(List<GameObject> gameObjects)
        {
            foreach(GameObject gameObject in gameObjects)
            {
                _maskedObjects.Remove(gameObject);
            }
        }

        /// <summary>
        /// Can be used to check if the specified game object is masked.
        /// </summary>
        public bool IsMasked(GameObject gameObject)
        {
            return _maskedObjects.Contains(gameObject);
        }

        /// <summary>
        /// Applies the specified object selection snapshot to the object selection.
        /// </summary>
        public void ApplySnapshot(ObjectSelectionSnapshot objectSelectionSnapshot)
        {
            // Apply the snapshot
            _selectedObjects = new HashSet<GameObject>(objectSelectionSnapshot.SelectedGameObjects);
            _lastSelectedGameObject = objectSelectionSnapshot.LastSelectedGameObject;

            // When the snapshot was applied the collection of selection objects was changed and
            // now the gizmos have to know about the new object collection.
            EditorGizmoSystem gizmoSystem = EditorGizmoSystem.Instance;
            ConnectObjectSelectionToGizmo(gizmoSystem.TranslationGizmo);
            ConnectObjectSelectionToGizmo(gizmoSystem.RotationGizmo);
            ConnectObjectSelectionToGizmo(gizmoSystem.ScaleGizmo);

            // Dispatch an object selection changed message
            ObjectSelectionChangedMessage.SendToInterestedListeners();
        }

        /// <summary>
        /// Applies the specified object selection mask snapshot. The method returns true if applying
        /// the mask snapshot causes the object selection to change. For example, if object A is selected,
        /// and the mask snapshot contains object A, the object A will have to be deselected. This will
        /// cause a selection change.
        /// </summary>
        public bool ApplyMaskSnapshot(ObjectSelectionMaskSnapshot objectSelectionMaskSnapshot)
        {
            // Apply the snapshot
            _maskedObjects = new HashSet<GameObject>(objectSelectionMaskSnapshot.MaskedObjects);

            // Now we have to make sure that if any of the masked objects are currently selected, we deselect them.
            bool selectionWasChanged = false;
            foreach(GameObject maskedObject in _maskedObjects)
            {
                // If selected, remove from selection
                if(IsGameObjectSelected(maskedObject))
                {
                    _selectedObjects.Remove(maskedObject);
                    selectionWasChanged = true;
                }
            }

            // If the selection has changed, send a message to all interested listeners
            if (selectionWasChanged) ObjectSelectionChangedMessage.SendToInterestedListeners();

            return selectionWasChanged;
        }

        /// <summary>
        /// Returns the selection's world center. This is the center of all selected objects'
        /// centers in world space.
        /// </summary>
        /// <remarks>
        /// If no object is currently selected, the method will return the zero vector.
        /// </remarks>
        public Vector3 GetSelectionWorldCenter()
        {
            // Recalculate the center and then return it to the caller
            RecalculateSelectionCenter();
            return _selectionWorldCenter;
        }

        /// <summary>
        /// A gizmo needs access to the selected objects collection so that it can transform
        /// the selected objects accordingly. This method can be called to connect the game
        /// object selection to the specified gizmo.
        /// </summary>
        public void ConnectObjectSelectionToGizmo(Gizmo gizmo)
        {
            gizmo.ControlledObjects = _selectedObjects;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Performs any necessary initializations.
        /// </summary>
        private void Start()
        {
            // Create the necessayr shaders and assign them to the interested entities
            _lineRenderingMaterial = new Material(Shader.Find("Selection Line Rendering"));
            _objectSelectionRectangle.BorderLineMaterial = _lineRenderingMaterial;
            _objectSelectionRectangle.FillMaterial = new Material(Shader.Find("2D Geometry Rendering"));
        }

        /// <summary>
        /// Called every frame to perform any necessary updates.
        /// </summary>
        private void Update()
        {
            // Update the mouse data for the current frame
            _mouse.UpdateInfoForCurrentFrame();

            // Was the left mouse button pressed?
            if (_mouse.WasLeftMouseButtonPressedInCurrentFrame) OnLeftMouseButtonDown();

            // Was the mouse moved since the last frame?
            if (_mouse.WasMouseMovedSinceLastFrame) OnMouseMoved();

            // Was the left mouse button released?
            if (_mouse.WasLeftMouseButtonReleasedInCurrentFrame) OnLeftMouseButtonUp();

            // Just make sure that we don't have a null reference inside the selected objects collection
            _selectedObjects.RemoveWhere(item => item == null);
        }

        /// <summary>
        /// This is called from 'Update' when the left mouse button is pressed during the current frame update.
        /// </summary>
        private void OnLeftMouseButtonDown()
        {
            // When the left mouse button is pressed, we will create a pre change snapshot in preparation
            // for a multi object selection session. When the left mouse button is released, if a multi
            // object selection was performed, this snapshot will be used to execute a post selection
            // changed action.
            _preMultiObjectSelectionChangeSnapshot = new ObjectSelectionSnapshot();
            _preMultiObjectSelectionChangeSnapshot.TakeSnapshot();

            // If the left mouse button was pressed in the current frame, attempt to pick a game object.
            // Note: We only do this if the active gizmo is not ready to manipulate game objects. In that case it
            //       means the user would like to use the gizmo to transform game objects and we don't want to
            //       change the object selection.
            if (!EditorGizmoSystem.Instance.IsActiveGizmoReadyForObjectManipulation()) CastRayForSingleObjectSelection();

            // We will always update the coordinates of the active selection shape when the left mouse button is pressed.
            // We will set both rectangle corners at the same value and we will adjust only one pair in the 'Update'
            // method when the mouse is moved while the left mouse button is pressed.
            _objectSelectionRectangle.SetEnclosingRectBottomRightPoint(Input.mousePosition);
            _objectSelectionRectangle.SetEnclosingRectTopLeftPoint(Input.mousePosition);

            // Update the visibility of the active object selection shape. Whenever the left mouse button was pressed,
            // we will mark the shape as visible if the gizmo is not ready to manipulate game objects.
            if (!EditorGizmoSystem.Instance.IsActiveGizmoReadyForObjectManipulation()) _objectSelectionRectangle.IsVisible = true;
            else _objectSelectionRectangle.IsVisible = false;
        }

        /// <summary>
        /// This is called from 'Update' when the left mouse button is released during the current frame update.
        /// </summary>
        private void OnLeftMouseButtonUp()
        {
            // The object selection area will always be hidden when the left mouse button is released
            _objectSelectionRectangle.IsVisible = false;

            // If a multi-object selection was performed since the left mouse button was pressed, we have to execute a
            // post selection changed action.
            if(_multiSelectionWasPerformedSinceLeftMouseButtonWasPressed)
            {
                // Create the post change snapshot
                _postMultiObjectSelectionChangeSnapshot = new ObjectSelectionSnapshot();
                _postMultiObjectSelectionChangeSnapshot.TakeSnapshot();

                // Execute the action using the pre and post change snapshots
                var action = new PostObjectSelectionChangedAction(_preMultiObjectSelectionChangeSnapshot, _postMultiObjectSelectionChangeSnapshot);
                action.Execute();

                // Reset data for the next multi object selection session
                _preMultiObjectSelectionChangeSnapshot = null;
                _postMultiObjectSelectionChangeSnapshot = null;
                _multiSelectionWasPerformedSinceLeftMouseButtonWasPressed = false;
            }
        }

        /// <summary>
        /// Called when the mouse was move since the last frame.
        /// </summary>
        private void OnMouseMoved()
        {
            if (_mouse.IsLeftMouseButtonDown)
            {
                // If the mouse was moved while holding down the left mouse button, we will update the object selection
                // shape coordinates and we will call 'HandleObjectSelectionWithActiveSelectionShape' to allow the user
                // to select/deselect multiple game objects at once using the active selection shape.
                _objectSelectionRectangle.SetEnclosingRectTopLeftPoint(Input.mousePosition);
                if (!EditorGizmoSystem.Instance.IsActiveGizmoReadyForObjectManipulation()) HandleObjectSelectionWithActiveSelectionShape();
            }
        }

        /// <summary>
        /// Allows the user to select/deselect multiple objects using the active selection shape.
        /// </summary>
        private void HandleObjectSelectionWithActiveSelectionShape()
        {
            // Retrieve the game objects which are visible to the camera and then check which of those objects are intersected
            // by the active selection shape. We will store the intersecting objects in 'intersectingGameObjects'.
            List<GameObject> objectsVisibleToCamera = EditorCamera.Instance.GetVisibleGameObjects();
            List<GameObject> intersectingGameObjects = _objectSelectionRectangle.GetIntersectingGameObjects(objectsVisibleToCamera, EditorCamera.Instance.Camera);

            // Normally, we will always clear the object selection and start anew. However, if the user wants to add
            // objects to the selection or deselect the already existing ones, we must keep the selection intact.
            if (!_addObjectsToSelectionEnabled && !_multiObjectDeselectEnabled)
            {
                // If the current number of selected objects is not 0, it means the selection will change when we clear it.
                _multiSelectionWasPerformedSinceLeftMouseButtonWasPressed = _selectedObjects.Count != 0;

                // Clear the object selection
                _selectedObjects.Clear();
            }

            // If the user doesn't want to deselect objects, we will add the intersected game objects to the selection
            if (!_multiObjectDeselectEnabled && intersectingGameObjects.Count != 0)
            {
                // At this point, we know for sure that the selection will change
                _multiSelectionWasPerformedSinceLeftMouseButtonWasPressed = true;

                // Add each game object to the selection and each time a new object is added, adjust the
                // last selected game object reference.
                // Note: Ensure that only game objects which are not masked are taken into account.
                foreach (GameObject gameObject in intersectingGameObjects)
                {
                    // Select only if not masked
                    if(!IsMasked(gameObject))
                    {
                        _selectedObjects.Add(gameObject);
                        _lastSelectedGameObject = gameObject;
                    }
                }
            }
            // If the user wants to deselect the intersected game objects, we will remove the objects from the selection
            else
            if (intersectingGameObjects.Count != 0)
            {
                // At this point, we know for sure that the selection will change
                _multiSelectionWasPerformedSinceLeftMouseButtonWasPressed = true;

                // Remove the objects from the selection and then update the last selected game object reference
                foreach (GameObject gameObject in intersectingGameObjects) _selectedObjects.Remove(gameObject);
                _lastSelectedGameObject = RetrieveAGameObjectFromObjectSelectionCollection();
            }
        }

        /// <summary>
        /// This method can be used to attempt to select/deselect an object in the scene using the current 
        /// mouse cursor position.
        /// </summary>
        private void CastRayForSingleObjectSelection()
        {
            // Ignore the call if the user is deselecting objects using the active selection shape
            if (_multiObjectDeselectEnabled) return;

            // Check if the mouse cursor picked a game object. If it did, we will call 'OnGameObjectWasPickedByMouseCursor'
            // to handle the pick operation. If no game object was picked, we will clear the game object selection. This is
            // standard behaviour for editor-like object selection functionality.
            RaycastHit rayHit;
            Camera camera = EditorCamera.Instance.Camera;
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out rayHit)) OnGameObjectWasPickedByMouseCursor(rayHit.collider.gameObject);
            else
            {
                // We will need to take a snapshot before the object selection changes (in case it has to change).
                // This will allow us to execute a post object selection changed action if needed.
                ObjectSelectionSnapshot preChangeSnapshot = null;

                // Clear the object selection if the user is not adding objects to the selection. This is an
                // important step. Imagine that the user would like to add multiple objects to the selection using 
                // the object selection area. In that case they would press the correct key on the keyboard and click 
                // and drag the mouse. If they click on an empty area and this check was not performed, the selection
                // would be cleared and the user would not be able to add multiple objects to the current selection.
                if (!_addObjectsToSelectionEnabled)
                {
                    // At this point we are bout to clear the object selection, but if the current number of
                    // selected objects is not 0, it means the selection will change. In this case, we will
                    // take a snapshot of the current object selection data.
                    if(_selectedObjects.Count != 0)
                    {
                        preChangeSnapshot = new ObjectSelectionSnapshot();
                        preChangeSnapshot.TakeSnapshot();
                    }

                    // Clear the selection
                    _selectedObjects.Clear();
                }

                // Has the object selection data changed?
                if(preChangeSnapshot != null)
                {
                    // Take a snapshot post change
                    ObjectSelectionSnapshot postChangeSnapshot = new ObjectSelectionSnapshot();
                    postChangeSnapshot.TakeSnapshot();

                    // Execute a post object selection change action
                    var action = new PostObjectSelectionChangedAction(preChangeSnapshot, postChangeSnapshot);
                    action.Execute();
                }
            }
        }

        /// <summary>
        /// Called whenever a game object is picked by the mouse cursor.
        /// </summary>
        /// <param name="pickedGameObject">
        /// This is the game object which was picked.
        /// </param>
        private void OnGameObjectWasPickedByMouseCursor(GameObject pickedGameObject)
        {
            // There is nothing to do if the game object is assigned to the selection mask
            if (IsMasked(pickedGameObject)) return;

            // If the selection changes in any way, this will hold the snapshot of the object
            // selection before the selection was changed. We need this information so that we
            // can execute a post object selection change action when the selection changes.
            ObjectSelectionSnapshot preChangeSnapshot = null;

            // We will need this to decide what to do with the picked game object
            if (_addObjectsToSelectionEnabled)
            {
                // If the game object is already selected, deselect it. Otherwise, add it to the current selection.
                // Note: In both cases, we will update the reference to the last selected game object accodingly.
                if (_selectedObjects.Contains(pickedGameObject))
                {
                    // Take a snapshot of the current object selection data and remove the picked game object
                    preChangeSnapshot = new ObjectSelectionSnapshot();
                    preChangeSnapshot.TakeSnapshot();
                    _selectedObjects.Remove(pickedGameObject);

                    // Note: There is no way to index a hash-set so that we can retrieve the game object that comes before the one
                    //       which was just removed. So we will call 'RetrieveAGameObjectFromObjectSelectionCollection' to give us
                    //       a game object which resides inside the collection.
                    _lastSelectedGameObject = RetrieveAGameObjectFromObjectSelectionCollection();
                }
                else
                {
                    // Take snapshot 
                    preChangeSnapshot = new ObjectSelectionSnapshot();
                    preChangeSnapshot.TakeSnapshot();

                    // Add the game object to the selection and update the last selected game object reference
                    _selectedObjects.Add(pickedGameObject);
                    _lastSelectedGameObject = pickedGameObject;
                }
            }
            else
            {
                // Take snapshot
                preChangeSnapshot = new ObjectSelectionSnapshot();
                preChangeSnapshot.TakeSnapshot();

                // In this case we want to clear the object selection and select only the game
                // object which was picked by the mouse cursor.
                _selectedObjects.Clear();
                _selectedObjects.Add(pickedGameObject);

                // Adjust the last picked game object
                _lastSelectedGameObject = pickedGameObject;
            }

            // Has the selection changed?
            if(preChangeSnapshot != null)
            {
                // Take a snapshot post change
                ObjectSelectionSnapshot postChangeSnapshot = new ObjectSelectionSnapshot();
                postChangeSnapshot.TakeSnapshot();

                // Execute a post object selection change action
                var action = new PostObjectSelectionChangedAction(preChangeSnapshot, postChangeSnapshot);
                action.Execute();
            }
        }

        /// <summary>
        /// This method is called after the camera has finished rendering the scene. 
        /// It allows us to perform any necessary drawing.
        /// </summary>
        private void OnRenderObject()
        {
            // Draw the object selection boxes
            ObjectSelectionBoxDrawer objectSelectionBoxDrawer = ObjectSelectionBoxDrawerFactory.CreateObjectSelectionBoxDrawer(_objectSelectionBoxDrawSettings.SelectionBoxStyle);
            objectSelectionBoxDrawer.DrawObjectSelectionBoxes(_selectedObjects, _objectSelectionBoxDrawSettings, _lineRenderingMaterial);

            // Draw the selection shape
            _objectSelectionRectangle.Draw();
        }

        /// <summary>
        /// Recalculates the object selection world center.
        /// </summary>
        private void RecalculateSelectionCenter()
        {
            // If no objects are selected, we will use the zero vector
            if (_selectedObjects.Count == 0) _selectionWorldCenter = Vector3.zero;
            else
            {
                // Calculate the sum of all objects' world centers
                Vector3 objectCenterSum = Vector3.zero;
                foreach (GameObject selectedObject in _selectedObjects) objectCenterSum += selectedObject.GetWorldCenter();

                // Now calculate the average and store it inside the '_selectionWorldCenter' variable
                _selectionWorldCenter = objectCenterSum / _selectedObjects.Count;
            }
        }

        /// <summary>
        /// Returns a game object which resides inside the object selection collections. This method
        /// is necessary because we are using a hash-set to store the selected objects and we can not
        /// index it as we would a list or array. If no selected objects are available, the method will
        /// return null.
        /// </summary>
        private GameObject RetrieveAGameObjectFromObjectSelectionCollection()
        {
            // Just get the first object from the set
            foreach (GameObject selectedGameObject in _selectedObjects)
            {
                return selectedGameObject;
            }

            // If there are no selected game objects, return null
            return null;
        }
        #endregion
    }
}
