﻿using UnityEngine;
using System.Collections.Generic;

namespace RTEditor
{
    /// <summary>
    /// This class can be used to draw object selection boxes for a group
    /// of selected objects using the 'ObjectSelectionBoxStyle.CornerLines' style.
    /// </summary>
    public class CornerLinesObjectSelectionBoxDrawer : ObjectSelectionBoxDrawer
    {
        #region Public Methods
        /// <summary>
        /// Draws the selection boxes for the specified selected game objects.
        /// </summary>
        /// <param name="selectedObjects">
        /// The selected objects whose selection boxes must be drawn.
        /// </param>
        /// <param name="objectSelectionBoxDrawSettings">
        /// The object selection box draw settings which control the way in which the
        /// selection boxes are drawn.
        /// </param>
        /// <param name="objectSelectionBoxLineMaterial">
        /// The material which must be used to render the object selection box lines.
        /// </param>
        public override void DrawObjectSelectionBoxes(HashSet<GameObject> selectedObjects, ObjectSelectionBoxDrawSettings objectSelectionBoxDrawSettings, Material objectSelectionBoxLineMaterial)
        {
            // Gather all object model space AABBs into a list. This will allow us to render the
            // selection boxes in one single 'SetPass' call.
            var modelSpaceAABBs = new List<Bounds>(selectedObjects.Count);
            var objectWorldTransforms = new List<Matrix4x4>(selectedObjects.Count);
            foreach (GameObject selectedObject in selectedObjects)
            {
                // Get the models space AABB of the game object. If it is valid, we will store the
                // game object and its world transform in the 2 lists.
                Bounds modelSpaceAABB = selectedObject.GetModelSpaceAABB();
                if (modelSpaceAABB.IsValid())
                {
                    modelSpaceAABBs.Add(selectedObject.GetModelSpaceAABB());
                    objectWorldTransforms.Add(selectedObject.transform.localToWorldMatrix);
                }
            }

            // Draw the boxes' corner lines
            GLPrimitives.DrawCornerLinesForBoxes(modelSpaceAABBs, objectWorldTransforms, objectSelectionBoxDrawSettings.SelectionBoxScaleFactor,
                                                 objectSelectionBoxDrawSettings.SelectionBoxCornerLineLength, EditorCamera.Instance.Camera, objectSelectionBoxDrawSettings.SelectionBoxLineColor, objectSelectionBoxLineMaterial);
        }
        #endregion
    }
}
