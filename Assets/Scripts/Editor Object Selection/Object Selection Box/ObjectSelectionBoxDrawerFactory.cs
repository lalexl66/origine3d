﻿using UnityEngine;

namespace RTEditor
{
    /// <summary>
    /// Factory class which can be used to create object selection box
    /// drawer entities based on a specified object selection box style.
    /// </summary>
    public static class ObjectSelectionBoxDrawerFactory
    {
        #region Public Static Functions
        /// <summary>
        /// Creates and returns an object selection box drawer entity based on 
        /// the specified object selection box style.
        /// </summary>
        public static ObjectSelectionBoxDrawer CreateObjectSelectionBoxDrawer(ObjectSelectionBoxStyle objectSelectionBoxStyle)
        {
            switch (objectSelectionBoxStyle)
            {
                case ObjectSelectionBoxStyle.CornerLines:

                    return new CornerLinesObjectSelectionBoxDrawer();

                case ObjectSelectionBoxStyle.WireBox:

                    return new WireObjectSelectionBoxDrawer();

                default:

                    return null;
            }
        }
        #endregion
    }
}
