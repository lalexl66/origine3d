﻿using UnityEngine;

namespace RTEditor
{
    /// <summary>
    /// This is a static class that can be used to apply zoom to a camera.
    /// </summary>
    public static class EditorCameraZoom
    {
        #region Public Static Functions
        /// <summary>
        /// Zooms the specified camera by the specified amount.
        /// </summary>
        /// <param name="camera">
        /// The camera which must be zoomed.
        /// </param>
        /// <param name="zoomAmount">
        /// The zoom amount. Positive values will zoom in and negative values will zoom out.
        /// </param>
        public static void ZoomCamera(Camera camera, float zoomAmount)
        {
            // For an orthographic camera, when zooming, we also have to change the size of the view volume.
            // This is because when an orthographic camera is used, objects don't get bigger or smaller as they
            // are moved towards or away from the camera. 
            float zoomAmountScale = 1.0f;
            if (camera.orthographic)
            {
                // We will use a minimum value for the orthographic size. This is because if we allow the size
                // to become < than 0, the scene will be inverted. Having it set to 0, is also not good because
                // exceptions will be thrown.
                const float minOrthoSize = 0.000001f;

                // Calculate the new ortho size 
                float newOrthoSize = camera.orthographicSize - zoomAmount;

                // Is the new ortho size < than the allowed minimum?
                // Note: If it is, what we would normally have to do is to just clamp the size to the
                //       minimum value. However, there is another thing that we must do. At the end
                //       of the function, we will also modify the position of the camera. But if the 
                //       zoom that is applied to the ortho size produces a size smaller than the allowed
                //       minimum, we have to also modify the amount of camera movement. The next lines
                //       of code will calculate a scale factor that is used to scale the 'zoomAmount'
                //       variable so that the position of the camera is adjusted as much as necessary.
                //       For example, if the current view size is equal to the allowed minimum, applying
                //       further zoom must not be allowed, so the code will calculate a zoom scale amount
                //       of 0, which means that the zoom will be canceled and the position of the camera
                //       will not be modified further.
                if(newOrthoSize < minOrthoSize)
                {
                    float delta = minOrthoSize - newOrthoSize;                  // Holds the amount which must be subtracted from the zoom
                    float percentageOfRemovedZoom = delta / zoomAmount;         // Holds the percentage of zoom which was removed

                    // Clamp the new ortho size to the allowed minimum
                    newOrthoSize = minOrthoSize;

                    // Calculate the zoom scale factor. We start from 1 and subtract the
                    // percentage which was removed earlier.
                    zoomAmountScale = 1.0f - percentageOfRemovedZoom;
                }

                // Set the new ortho size and scale the zoom amount
                camera.orthographicSize = Mathf.Max(minOrthoSize, newOrthoSize);
                zoomAmount *= zoomAmountScale;
            }

            // Regardless of the type of camera we are using, we will also move the position of the camera. It may appear
            // strange that we are doing this for an orthographic camera also but this is actually necessary because a camera
            // has a near and a far clip plane. If we don't move the camera along its look vector, objects may get clipped
            // by the near or far clip plane regardless of the zoom factor that is applied to the camera.
            Transform cameraTransform = camera.transform;
            cameraTransform.position += cameraTransform.forward * zoomAmount;
        }
        #endregion
    }
}
