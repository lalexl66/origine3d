﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace RTEditor
{
    /// <summary>
    /// Custom inspector implementation for the 'EditorObjectSelection' class.
    /// </summary>
    [CustomEditor(typeof(EditorObjectSelection))]
    public class EditorObjectSelectionInspectorGUI : Editor
    {
        #region Private Static Variables
        /// <summary>
        /// The following variables control the visibility for different categories of settings.
        /// </summary>
        static bool _objectSelectionBoxSettingAreVisible = true;
        static bool _objectSelectionBoxDrawSettingsAreVisible = true;
        static bool _objectSelectionRectangleSettingsAreVisible = true;
        static bool _objectSelectionRectangleDrawSettingsAreVisible = true;
        #endregion

        #region Private Variables
        /// <summary>
        /// Reference to the editor object selection module.
        /// </summary>
        private EditorObjectSelection _editorObjectSelection;
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the inspector needs to be rendered.
        /// </summary>
        public override void OnInspectorGUI()
        {
            const int indentLevel = 1;

            // Let the user modify the object selection box settings
            _objectSelectionBoxSettingAreVisible = EditorGUILayout.Foldout(_objectSelectionBoxSettingAreVisible, "Object Selection Box Settings");
            if(_objectSelectionBoxSettingAreVisible)
            {
                // Let the user modify the object selection box draw settings
                EditorGUI.indentLevel += indentLevel;
                _objectSelectionBoxDrawSettingsAreVisible = EditorGUILayout.Foldout(_objectSelectionBoxDrawSettingsAreVisible, "Draw Settings");
                if(_objectSelectionBoxDrawSettingsAreVisible)
                {
                    // Let the user choose the object selection box style
                    ObjectSelectionBoxDrawSettings objectSelectionBoxDrawSettings = _editorObjectSelection.ObjectSelectionBoxDrawSettings;
                    ObjectSelectionBoxStyle newObjectSelectionBoxStyle = (ObjectSelectionBoxStyle)EditorGUILayout.EnumPopup("Selection Box Style", objectSelectionBoxDrawSettings.SelectionBoxStyle);
                    if(newObjectSelectionBoxStyle != objectSelectionBoxDrawSettings.SelectionBoxStyle)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                        objectSelectionBoxDrawSettings.SelectionBoxStyle = newObjectSelectionBoxStyle;
                    }

                    // If the object selection box style is set to 'CornerLines', let the user choose the length of the corner lines
                    float newFloatValue;
                    if(objectSelectionBoxDrawSettings.SelectionBoxStyle == ObjectSelectionBoxStyle.CornerLines)
                    {
                        newFloatValue = EditorGUILayout.FloatField("Corner Line Length", objectSelectionBoxDrawSettings.SelectionBoxCornerLineLength);
                        if(newFloatValue != objectSelectionBoxDrawSettings.SelectionBoxCornerLineLength)
                        {
                            UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                            objectSelectionBoxDrawSettings.SelectionBoxCornerLineLength = newFloatValue;
                        }
                    }

                    // Let the user choose the selection box line color
                    Color newColorValue = EditorGUILayout.ColorField("Selection Box Line Color", objectSelectionBoxDrawSettings.SelectionBoxLineColor);
                    if(newColorValue != objectSelectionBoxDrawSettings.SelectionBoxLineColor)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                        objectSelectionBoxDrawSettings.SelectionBoxLineColor = newColorValue;
                    }

                    // Let the user choose the selection box scale factor
                    newFloatValue = EditorGUILayout.FloatField("Selection Box Scale Factor", objectSelectionBoxDrawSettings.SelectionBoxScaleFactor);
                    if(newFloatValue != objectSelectionBoxDrawSettings.SelectionBoxScaleFactor)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                        objectSelectionBoxDrawSettings.SelectionBoxScaleFactor = newFloatValue;
                    }
                }
                EditorGUI.indentLevel -= indentLevel;
            }

            // Let the user modify the object selection rectangle settings
            _objectSelectionRectangleSettingsAreVisible = EditorGUILayout.Foldout(_objectSelectionBoxSettingAreVisible, "Object Selection Rectangle Settings");
            if(_objectSelectionRectangleSettingsAreVisible)
            {
                // Let the user modify the object selection rectangle draw settings
                EditorGUI.indentLevel += indentLevel;
                _objectSelectionRectangleDrawSettingsAreVisible = EditorGUILayout.Foldout(_objectSelectionRectangleDrawSettingsAreVisible, "Draw Settings");
                if(_objectSelectionRectangleDrawSettingsAreVisible)
                {
                    // Let the user modify the object selection border line color
                    ObjectSelectionRectangleDrawSettings objectSelectionRectangleDrawSettings = _editorObjectSelection.ObjectSelectionRectangleDrawSettings;
                    Color newColorValue = EditorGUILayout.ColorField("Border Line Color", objectSelectionRectangleDrawSettings.BorderLineColor);
                    if(newColorValue != objectSelectionRectangleDrawSettings.BorderLineColor)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                        objectSelectionRectangleDrawSettings.BorderLineColor = newColorValue;
                    }

                    // Let the user modify the object selection rectangle fill color
                    newColorValue = EditorGUILayout.ColorField("Fill Color", objectSelectionRectangleDrawSettings.FillColor);
                    if(newColorValue != objectSelectionRectangleDrawSettings.FillColor)
                    {
                        UnityEditorUndoHelper.RecordObjectForInspectorPropertyChange(_editorObjectSelection);
                        objectSelectionRectangleDrawSettings.FillColor = newColorValue;
                    }
                }
                EditorGUI.indentLevel -= indentLevel;
            }
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Called when the editor object selection object is selected in the scene view.
        /// </summary>
        protected virtual void OnEnable()
        {
            _editorObjectSelection = target as EditorObjectSelection;
        }
        #endregion
    }
}
#endif
