﻿using UnityEngine;
using System.Collections.Generic;

namespace RTEditor
{
    /// <summary>
    /// This class acts as a base abstract class for all classes which handle
    /// object selection box drawing.
    /// </summary>
    public abstract class ObjectSelectionBoxDrawer
    {
        #region Public Abstract Methods
        /// <summary>
        /// Draws the selection boxes for the specified selected game objects. Must
        /// be implemented in all derived classes.
        /// </summary>
        /// <param name="selectedObjects">
        /// The selected objects whose selection boxes must be drawn.
        /// </param>
        /// <param name="objectSelectionBoxDrawSettings">
        /// The object selection box draw settings which control the way in which the
        /// selection boxes are drawn.
        /// </param>
        /// <param name="objectSelectionBoxLineMaterial">
        /// The material which must be used to render the object selection box lines.
        /// </param>
        public abstract void DrawObjectSelectionBoxes(HashSet<GameObject> selectedObjects, ObjectSelectionBoxDrawSettings objectSelectionBoxDrawSettings, Material objectSelectionBoxLineMaterial);
        #endregion
    }
}
