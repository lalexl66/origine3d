﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace RTEditor
{
    /// <summary>
    /// Implements the functionality for a runtime editor camera.
    /// </summary>
    public class EditorCamera : MonoSingletonBase<EditorCamera>
    {
        #region Private Variables
        /// <summary>
        /// Holds all zoom related settings.
        /// </summary>
        [SerializeField]
        private EditorCameraZoomSettings _zoomSettings = new EditorCameraZoomSettings();

        /// <summary>
        /// Holds all pan related settings.
        /// </summary>
        [SerializeField]
        private EditorCameraPanSettings _panSettings = new EditorCameraPanSettings();

        /// <summary>
        /// This is the camera rotation speed expressed in degrees/second.
        /// </summary>
        [SerializeField]
        private float _rotationSpeedInDegrees = 8.8f;

        /// <summary>
        /// Cached camera component for easy access.
        /// </summary>
        private Camera _camera;

        /// <summary>
        /// We will need to have access to mouse information.
        /// </summary>
        private Mouse _mouse = new Mouse();

        /// <summary>
        /// If the application is running in windowed mode, the user may press the left mouse button outside
        /// the application window causing the application to loose focus. The problem is that the focus will
        /// only be gained when the user presses a mouse button inside the application window client area. The
        /// 'Update' method of the monobehaviour will not be called while the app doesn't have focus and the 
        /// mouse cursor position in previous frame is not updated accordingly. The moment the user presses
        /// a mouse button inside the window client area, the mouse cursor move offset will be calculated but
        /// will contain wild values which will cause the camera position/orientation to snap. This effect is 
        /// totally undesirable and it can also happen while running the application inside the Unity Editor.
        /// So, every time the application gains focus, we will set this variable to true, so that we can adjust
        /// the mouse cursor position in previous frame properly inside the 'Update' method.
        /// </summary>
        private bool _applicationJustGainedFocus;
        #endregion

        #region Public Static Properties
        /// <summary>
        /// Returns the minimum value that the camera rotation speed can have.
        /// </summary>
        public static float MinRotationSpeedInDegrees { get { return 0.01f; } }
        #endregion

        #region Public Properties
        /// <summary>
        /// Returns the zoom settings.
        /// </summary>
        public EditorCameraZoomSettings ZoomSettings { get { return _zoomSettings; } }

        /// <summary>
        /// Returns the pan settings.
        /// </summary>
        public EditorCameraPanSettings PanSettings { get { return _panSettings; } }

        /// <summary>
        /// Gets/sets the camera rotation speed in degrees. The minimum value that this property can have is given by the 
        /// 'MinRotationSpeedInDegrees' property. Values smaller than that will be clamped accordingly.
        /// </summary>
        public float RotationSpeedInDegrees { get { return _rotationSpeedInDegrees; } set { _rotationSpeedInDegrees = Mathf.Max(value, MinRotationSpeedInDegrees); } }

        /// <summary>
        /// Returns the 'Camera' component.
        /// </summary>
        public Camera Camera { get { return _camera; } }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the game objects which are visible to the camera.
        /// </summary>
        /// <remarks>
        /// This method detects only objects which have a collider attached to them.
        /// </remarks>
        public List<GameObject> GetVisibleGameObjects()
        {
            return _camera.GetVisibleGameObjects();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Called when the game object is created and we will use this to 
        /// perform any necessary initializations.
        /// </summary>
        private void Awake()
        {
            // Cache needed data.
            // Note: If a camera component wasn't attached, we will create one.
            _camera = this.gameObject.GetComponent<Camera>();
            if (_camera == null) _camera = this.gameObject.AddComponent<Camera>();
        }

        /// <summary>
        /// Called every frame to perform any necessary updates.
        /// </summary>
        private void Update()
        {
            // Reset the mouse cursor position in the previous frame in order to make sure we don't 
            // get wild offset values which will cause unwanted effects.
            if (_applicationJustGainedFocus)
            {
                _applicationJustGainedFocus = false;
                _mouse.ResetCursorPositionInPreviousFrame();
            }

            // Make sure the mouse has its information updated for the current frame update
            _mouse.UpdateInfoForCurrentFrame();

            ApplyCameraZoomBasedOnUserInput();
            PanCameraBasedOnUserInput();
            RotateCameraBasedOnUserInput();
        }

        /// <summary>
        /// Applies any necessary zoom to the camera based on user input. 
        /// </summary>
        private void ApplyCameraZoomBasedOnUserInput()
        {
            // Zoom if necessary
            float scrollSpeed = Input.GetAxis("Mouse ScrollWheel");
            if(scrollSpeed != 0.0f)
            {
                // Zoom based on the active zoom mode
                if (_zoomSettings.ZoomMode == EditorCameraZoomMode.Standard)
                {
                    // Note: We will use the mouse scroll wheel for zooming and we will establish
                    //       the zoom speed based on the camera type.
                    float zoomSpeed = _camera.orthographic ? _zoomSettings.OrthographicStandardZoomSpeed : _zoomSettings.PerspectiveStandardZoomSpeed * Time.deltaTime;
                    EditorCameraZoom.ZoomCamera(_camera, scrollSpeed * zoomSpeed);
                }
                else
                {
                    StopCoroutine("StartSmoothZoom");
                    StartCoroutine(StartSmoothZoom());
                }
            }
        }

        /// <summary>
        /// Pans the camera based on user input.
        /// </summary>
        private void PanCameraBasedOnUserInput()
        {
            // Only pan if the middle mouse button is down
            if (_mouse.IsMiddleMouseButtonDown && _mouse.WasMouseMovedSinceLastFrame)
            {
                if(_panSettings.PanMode == EditorCameraPanMode.Standard)
                {
                    float panSpeedTimesDeltaTime = Time.deltaTime * _panSettings.StandardPanSpeed;
                    EditorCameraPan.PanCamera(_camera,
                                              -_mouse.CursorOffsetSinceLastFrame.x * panSpeedTimesDeltaTime,
                                              -_mouse.CursorOffsetSinceLastFrame.y * panSpeedTimesDeltaTime);
                }
                else
                {
                    StopCoroutine("StartSmoothPan");
                    StartCoroutine(StartSmoothPan());
                }
            }
        }

        /// <summary>
        /// Rotates the camera based on user input.
        /// </summary>
        private void RotateCameraBasedOnUserInput()
        {
            // Only rotate if the right mouse button is pressed
            if (_mouse.IsRightMouseButtonDown)
            {
                float rotationSpeedTimesDeltaTime = _rotationSpeedInDegrees * Time.deltaTime;
                EditorCameraRotation.RotateCamera(_camera,
                                                  -_mouse.CursorOffsetSinceLastFrame.y * rotationSpeedTimesDeltaTime,
                                                  _mouse.CursorOffsetSinceLastFrame.x * rotationSpeedTimesDeltaTime);
            }
        }

        /// <summary>
        /// Called when the focus of the application window changes.
        /// </summary>
        private void OnApplicationFocus(bool focusStatus)
        {
            // If the application gained focus, store this state in the '_applicationJustGainedFocus' variable.
            // We will need this information inside the 'Update' method to make sure that the mouse cursor
            // move offset doesn't contain wild values.
            // Note: Settings the mouse cursor position here doesn't seem to produce the correct results.
            if (focusStatus) _applicationJustGainedFocus = true;
        }
        #endregion

        #region Coroutines
        /// <summary>
        /// Starts a smooth zoom operation.
        /// </summary>
        private IEnumerator StartSmoothZoom()
        {
            // Calculate the camera initial speed and the smooth value based on the camera type
            float currentSpeed = (_camera.orthographic ? _zoomSettings.OrthographicSmoothZoomSpeed : _zoomSettings.PerspectiveSmoothZoomSpeed) * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime;
            float smoothValue = _camera.orthographic ? _zoomSettings.OrthographicSmoothValue : _zoomSettings.PerspectiveSmoothValue;

            while (true)
            {
                // Zoom the camera using the current speed
                EditorCameraZoom.ZoomCamera(_camera, currentSpeed);

                // Move from the current speed towards 0 using the smooth value
                currentSpeed = Mathf.Lerp(currentSpeed, 0.0f, smoothValue);

                // Exit if the speed is small enough
                if (Mathf.Abs(currentSpeed) < 1e-5f) break;

                // Wait for the next frame
                yield return null;
            }
        }

        /// <summary>
        /// Starts a smooth pan operation.
        /// </summary>
        private IEnumerator StartSmoothPan()
        {
            // Calculate the camera initial speed and store the smooth value
            float panSpeedTimesDeltaTime = Time.deltaTime * _panSettings.SmoothPanSpeed;
            float panSpeedRightAxis = -_mouse.CursorOffsetSinceLastFrame.x * panSpeedTimesDeltaTime;
            float panSpeedUpAxis = -_mouse.CursorOffsetSinceLastFrame.y * panSpeedTimesDeltaTime;
            float smoothValue = _panSettings.SmoothValue;

            while (true)
            {
                // Pan the camera using the current speed along the camera right and up axes
                EditorCameraPan.PanCamera(_camera, panSpeedRightAxis, panSpeedUpAxis);

                // Move from the current speed towards 0 using the smooth value
                panSpeedRightAxis = Mathf.Lerp(panSpeedRightAxis, 0.0f, smoothValue);
                panSpeedUpAxis = Mathf.Lerp(panSpeedUpAxis, 0.0f, smoothValue);

                // Exit if both speed values are small enough
                if (Mathf.Abs(panSpeedRightAxis) < 1e-5f && Mathf.Abs(panSpeedUpAxis) < 1e-5f) break;

                // Wait for the next frame
                yield return null;
            }
        }
        #endregion
    }
}
