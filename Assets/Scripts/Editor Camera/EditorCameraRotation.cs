﻿using UnityEngine;
using System.Collections;

namespace RTEditor
{
    /// <summary>
    /// This is a static class that can be used to rotate a camera.
    /// </summary>
    public static class EditorCameraRotation
    {
        #region Public Static Functions
        /// <summary>
        /// Rotates the specified camera using the specified rotation amounts.
        /// </summary>
        /// <remarks>
        /// The function will rotate around the camera right axis first and then around
        /// the global Y axis.
        /// </remarks>
        /// <param name="camera">
        /// The camera which must be rotated.
        /// </param>
        /// <param name="rotationCameraRight">
        /// The amount of rotation in degrees which must be applied around the camera right axis.
        /// </param>
        /// <param name="rotationGlobalUp">
        /// The amount of rotation in degrees which must be applied around the global Y axis.
        /// </param>
        public static void RotateCamera(Camera camera, float rotationCameraRight, float rotationGlobalUp)
        {
            // Rotate around the camera right axis and the global Y axis respectively
            Transform cameraTransform = camera.transform;
            cameraTransform.Rotate(cameraTransform.right, rotationCameraRight, Space.World);
            cameraTransform.Rotate(Vector3.up, rotationGlobalUp, Space.World);
        }
        #endregion
    }
}
